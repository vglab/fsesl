package main

import (
	"bufio"
	"bytes"
	"fmt"
	"log"
	"net"
	"strconv"
	"strings"
	"time"
)

// ESLConnection xxx
type ESLConnection struct {
	addr       string
	pass       string
	hostUUID   string
	subscribed map[ESLEventName]bool
	Requests   chan ESLRequest
	Events     chan *ESLEvent
	Stop       chan bool
	Done       chan bool
	Error      chan error
	log        *log.Logger
}

// ESLResult xxx
type ESLResult struct {
	Succ bool
	Text string
	Data string
	Rqst ESLRequest
}

func NewESLConnection(host string, port int, pass string) *ESLConnection {
	if host == "" {
		host = "127.0.0.1"
	}
	if port <= 0 {
		port = 8021
	}
	if pass == "" {
		pass = "ClueCon"
	}
	addr := fmt.Sprintf("%s:%d", host, port)

	conn := &ESLConnection{
		addr:       addr,
		pass:       pass,
		hostUUID:   "",
		log:        log.Default(),
		subscribed: map[ESLEventName]bool{},
		Requests:   make(chan ESLRequest),
		Events:     make(chan *ESLEvent, 50),
		Stop:       make(chan bool),
		Done:       make(chan bool),
		Error:      make(chan error),
	}
	println("eslc: new.... starting conn")
	go conn.run()
	return conn
}

func (c *ESLConnection) run() {
	rdr := &ESLReader{
		tcp:      nil,
		replies:  make(chan *ESLResult),
		sendPass: make(chan bool),
		reset:    make(chan bool),
		stop:     false,
		done:     make(chan bool),
		errors:   make(chan error),
	}
	println("eslc: conn... starting reader")
	go rdr.run(c.Events, c.addr)

	var request ESLRequest
	var timeout <-chan time.Time

	for {
		outer:
		// All code here in the outer loop (before entering inner loop) sets up the state machine for authentication.
		var reqs chan ESLRequest	// Nil at first. Can't take requests until authenticated with FS.
		var repl chan *ESLResult// Nil at first. Once authenticated, will be the replies coming up from the reader.

		auth := rdr.replies		// When we're non-authenticated, we want the reader reply (to the auth req) to get handled differently.

		// This is the inner loop that continues to run while the TCP connection is up and good.
		for {
			select {
			case <-timeout:
				println("eslc: conn... timeout, closing TCP")
				// We think something may be wrong because we took too long to get a response. Break the connection,
				// and the reader will recreate a new connection, and prompt us to resend authentication.
				rdr.tcp.Close()
				goto outer
				
			case <-rdr.sendPass:
				println("eslc: conn... reader asking for password")
				// Step one - reader (re)created TCP connection to FS, now is ready to send authentication.
				// We are asked for the password so that we can start the timer.
				timeout = time.After(30 * time.Second)
				rdr.sendAuth(c.pass)

			case reply := <-auth:
				println("eslc: conn... reader returning auth reply")
				// Here, "auth" is the reply channel from the reader, just like "repl" is the same reply channel.
				// But we use the "auth" variable so we know we're receiving a reply to an authentication request
				// instead of a reply to a command request. Notice that we handle an authentication reply differently.
				timeout = nil
				// Step two - the reply to the auth request we sent out. Did we authenticate?
				if reply.Succ {
					println("eslc: conn... auth successful")
					auth = nil        	// Success - set auth to nil - Now we read from rdrReplies channel on repl instead of auth (actually same channel).
					repl = rdr.replies 	// Read replies from repl instead of auth (same underlying channel).
					reqs = c.Requests	// Can now start taking in requests from parent.

					if request != nil {
						println("eslc: conn... sending queued request")
						// If this is a reconnect/reauth from a prior attempt to send a command, resend the command now that we reconnected.
						c.Requests <- request  // Minor issue here - if another request already queued, will now be out of order.
						request = nil
					}
				} else {
					println("eslc: conn... auth failed, closing TCP")
					rdr.tcp.Close() // Will trigger an error in the reader, and the reader will re-open connection.
					goto outer		// To outer loop, where we reset to handle retrying authentication.
				}

			case request = <-reqs:
				println("eslc: conn... received client request")
				// Read commands sent down to us. If we're already in the middle of a command, this will be a nil channel and not readable.
				reqs = nil // Can only read one command at a time.
				if request.Timeout() > 0 {
					timeout = time.After(request.Timeout() * time.Second)
				}
				rdr.sendAPI(request)

			case reply := <-repl:
				println("eslc: conn... reader returns reply to request")
				timeout = nil
				reqs = c.Requests // Request is done - now can read next request from parent.
				reply.Rqst = request
				request.Done() <- reply
				request = nil

			case err := <-rdr.errors:
				println("eslc: conn... reader returns error")
				c.Error <-err

			case <-c.Stop:
				println("eslc: conn... received stop signal")
				// If we're told to stop, signal this to the tcp reader by shutting down its connection.
				rdr.stop = true	// Tells the reader that when it gets the socket error it should send a done event and return.
				reqs = nil 		// Can't read any more commands from parent.
				if rdr.tcp != nil {
					rdr.tcp.Close()	// Breaks the reader out of reading the socket.
				}
				request = nil

			case <-rdr.done:
				println("eslc: conn... reader is done, returning")
				// The reader only sends this (and only exits/returns) when we tell it to stop. If it gets errors it just keeps retrying.
				c.Done <-true
				return
			}
		}
	}
}

func (c *ESLConnection) Channel(uuid string, add bool) error {
	return c.Filter("Unique-ID", uuid, add)
}

// CreateChannel xxx
func (c *ESLConnection) CreateChannel() (string, error) {
	req := NewAPIRequest("create_uuid", nil)
	c.Requests <- req
	select {
	case res := <-req.done:
		if res.Succ {
			uuid := res.Text
			return uuid, nil
		} else {
			return "", fmt.Errorf(res.Text)
		}
	}
}

func (c *ESLConnection) SetEvents(add bool, events ...ESLEventName) error {
	return c.sendEventRequest(events, add)
}

func (c *ESLConnection) sendEventRequest(events []ESLEventName, add bool) error {
	// Look at the events we've been given, and put them into a local set if they are currently subscribed to on this connection.
	// Use a map(set) to eliminate any accidental duplicates sent in from the caller.
	eventSet := make(map[ESLEventName]bool)
	for _, eventName := range events {
		if _, found := c.subscribed[eventName]; !found {
			eventSet[eventName] = true
		}
	}
	// Send the event(s) subscription request to the host.
	eventReq := NewEventRequest(eventSet, add)
	eventRes := c.sendRequest(eventReq, time.Duration(3)*time.Second)
	
	// If the request succeeded, add to the subscribed events list.
	if eventRes.Succ {
		for eventName := range eventSet {
			if add {
				c.subscribed[eventName] = true
			} else {
				delete(c.subscribed, eventName)
			}
		}
		return nil
	}
	return fmt.Errorf(eventRes.Text)
}

func (c *ESLConnection) Execute(uuid, command string, args ...string) (string, string, error) {
	req := NewExecuteRequest(uuid, command, args)
	c.Requests <-req
	res := <-req.Done()
	if res.Succ {
		return res.Text, res.Data, nil
	}
	return res.Text, res.Data, fmt.Errorf(res.Text)
}

func (c *ESLConnection) Filter(name, value string, add bool) error {
	filterReq := NewFilterRequest(name, value, add)
	c.Requests <- filterReq
	filterRes := <-filterReq.Done()
	if filterRes.Succ {
		return nil
	}
	return fmt.Errorf(filterRes.Text)
}

func (c *ESLConnection) sendRequest(req ESLRequest, timeout time.Duration) *ESLResult {
	select {
	case c.Requests <-req:
		select {
		case res := <-req.Done():
			return res
		case <-time.After(timeout):
			return &ESLResult{Succ: false, Text: "timeout", Rqst: req}
		}
	case <-time.After(timeout):
		return &ESLResult{Succ: false, Text: "timeout", Rqst: req}
	}
}

type ESLReader struct {
	tcp 		*net.TCPConn
	replies		chan *ESLResult
	sendPass	chan bool
	reset		chan bool
	stop		bool
	done		chan bool
	errors		chan error
}

func (r *ESLReader) run(events chan *ESLEvent, addr string) {
	r.tcp, _ = newTCPConn(addr)
	for {
		for r.tcp == nil {
			println("eslc: read... TCP is nil")
			if r.stop {
				println("eslc: read... stop was requested, returning")
				r.done <-true
				return
			}
			var err error
			if r.tcp, err = newTCPConn(addr); err == nil {
				println("eslc: read... TCP connected")
				break
			} else {
				println("eslc: read... TCP not connected, sleeping")
				r.errors <-err
				time.Sleep(time.Duration(5)*time.Second)
				continue
			}
		}

		lr := bufio.NewReader(r.tcp)

		headers := make(map[string]string)
		buffer := bytes.Buffer{}
		started := false
		length := 0

		for {
			if line, _, err := lr.ReadLine(); err == nil {
				lineStr := string(line)
				// Identify blank lines. They're important.
				if len(lineStr) == 0 {
					if !started {
						// Got blank line, but haven't seen any text yet, this is a garbage blank line. Skip it.
						continue
					} else if length == 0 {
						// If we were in the msg, now a blank line, and no content-length to read, message completed.
						break
					} else {
						// Blank line, had content, and content-length > 0. Time to read the data!
						content := make([]byte, length)
						for {
							if bytesRead, err := lr.Read(content); err != nil {
								break
							} else {
								buffer.Write(content[:bytesRead])
								length -= bytesRead
								if length <= 0 {
									break
								}
								content = make([]byte, length)
							}
						}
						// If we get here, we're done with this message. Break out of this inner loop that reads this message, go below to process.
						break
					}
				} else {
					// We have a line of text. Make sure this flag is set. Then process the line. These must be headers, not content.
					started = true
					// Must (should) be header name/value pairs.
					hdrNameVal := strings.SplitN(lineStr, ":", 2)
					if len(hdrNameVal) == 2 {
						// Only process lines that look like valid headers. Might have to revisit this!
						hdr := strings.TrimSpace(hdrNameVal[0])
						val := strings.TrimSpace(hdrNameVal[1])
						// This header is special. We need it so we can read the upcoming content. Without the length, don't know how to read it.
						if hdr == "Content-Length" {
							if length, err = strconv.Atoi(val); err != nil {
								length = 0
							}
						}
						headers[hdr] = val
					}
				}
			} else {
				println("eslc: read... error reading, closing TCP")
				r.tcp.Close()
				r.tcp = nil
				break
			}
		}

		// When we fall out to here, we have the various fields filled in that make up the message we received.
		// Now figure out what it is so we can send it back correctly.

		if ctype, found := headers["Content-Type"]; !found {
			continue
		} else {
			// We're being asked for our password - happens at the start. Send back a sendPass trigger to our handler.
			// Our handler will send down the password, and we will then receive the response in a "command-reply".
			// We'll send that response to our handler, and our handler will decide if/when we are connected.
			if ctype == "auth/request" {
				r.sendPass <- true

				// "command/reply" only has header "Reply-Text" - no "Content-Length" or content following
			} else if ctype == "command/reply" {
				replyMsg := &ESLResult{}
				if replyText, found := headers["Reply-Text"]; found {
					replyMsg.Text, replyMsg.Succ = parseStatusLine(replyText)
				}
				r.replies <- replyMsg

			} else if ctype == "api/response" {
				replyMsg := &ESLResult{}
				if replyText, found := headers["Reply-Text"]; found {
					replyMsg.Text, replyMsg.Succ = parseStatusLine(replyText)
					if buffer.Len() > 0 {
						replyMsg.Data = buffer.String()
					}
				}
				r.replies <- replyMsg

				// With a plain text event, all name/value pairs are in the headers map already.
			} else if ctype == "text/event-plain" {
				eventMsg := ESLEvent{}
				eventMsg.Headers = headers
				eventMsg.Content = buffer.String()
				scanner := bufio.NewScanner(bytes.NewReader([]byte(eventMsg.Content)))
				for scanner.Scan() {
					lineStr := scanner.Text()
					hdrNameVal := strings.SplitN(lineStr, ":", 2)
					if len(hdrNameVal) == 2 {
						hdr := strings.TrimSpace(hdrNameVal[0])
						val := strings.TrimSpace(hdrNameVal[1])
						eventMsg.Headers[hdr] = val
						if hdr == "Event-Name" {
							eventMsg.Name = val
						}
					}
				}
				events <- &eventMsg

				// With a json event, content type and length are in headers, name/value pairs are in the content.
			} else if ctype == "text/json" || ctype == "application/json" {
				// Parse out json, add to headers
				eventMsg := ESLEvent{}
				eventMsg.Headers = headers
				eventMsg.Content = buffer.String()
				events <- &eventMsg

				// With an xml event, content type and length are in headers, name/value pairs are in the content.
			} else if ctype == "text/xml" {
				// Parse out xml, add to headers
				eventMsg := ESLEvent{}
				eventMsg.Headers = headers
				eventMsg.Content = buffer.String()
				events <- &eventMsg

			} else if ctype == "text/disconnect-notice" {
				// The channel has closed. We're done here. The receiver of the done event will take care of the socket.
				r.errors <-fmt.Errorf(ctype)
				r.tcp.Close()
				r.tcp = nil

			} else if ctype == "text/rude-rejection" {
				// On attempting to authenticate, the server can reject us because of our IP address. Since a new pass won't help, send back special failure.
				r.errors <-fmt.Errorf(ctype)
				r.tcp.Close()
				r.tcp = nil

			//} else if ctype == "log/data" {
				// For now, do nothing with log data. Hopefully we didn't register for it.
			}
		}
	}
}

func parseStatusLine(line string) (text string, succ bool) {
	tokens := strings.SplitN(line, " ", 2)
	if len(tokens) > 0 {
		if tokens[0] == "+OK" {
			succ = true
		} else if tokens[0] == "-ERR" {
			succ = false
		}
		if len(tokens) > 1 {
			text = tokens[1]
		}
	}
	return
}

func newTCPConn(hostPort string) (*net.TCPConn, error) {
	if tcpAddr, err := net.ResolveTCPAddr("tcp", hostPort); err == nil {
		if tcpConn, err := net.DialTCP("tcp", nil, tcpAddr); err != nil {
			return nil, err
		} else {
			return tcpConn, nil
		}
	} else {
		return nil, err
	}
}

func (r *ESLReader) sendAPI(rqst ESLRequest) error {
	commandLine := fmt.Sprintf("%s\r\n\r\n", rqst)
	println("eslc: read... (API)  sending=> %s", commandLine)
	if _, err := fmt.Fprintf(r.tcp, commandLine); err != nil {
		return err
	}
	return nil
}
func (r *ESLReader) sendAuth(pass string) error {
	commandLine := fmt.Sprintf("auth %s\r\n\r\n", pass)
	println("eslc: read... (auth) sending=> %s", commandLine)
	if _, err := fmt.Fprintf(r.tcp, commandLine); err != nil {
		return err
	}
	return nil
}


/****************** Events ***********************/

// ESLEvent xxx
type ESLEvent struct {
	Name    string
	Headers map[string]string
	Content string
}

// ESLVars defines the set of event header EventNames
var ESLVars = struct {
	EventName                 string
	FreeswitchIPV4            string
	UniqueID                  string
	AnswerState               string
	CallDirection             string
	CallerDirection           string
	CallerContext             string
	CallerDestNum             string
	CallerCalleeIDNum         string
	CallerCalleeIDName        string
	CallerCallerIDNum         string
	CallerCallerIDName        string
    CallerUsername            string
	VarDirection              string
	VarSipReqHost             string
	VarSipReqUser             string
	VarSipFmHost              string
	VarSipFmUser              string
	VarSipFmDisplay           string
	VarSipToHost              string
	VarSipToUser              string
	VarSipUserAgent           string
	VarEffCallerIDNum         string
	VarHangupCause            string
	VarSipHangupDisp          string
	VarBridgeHangupCause      string
	EventTime                 string
	CallerChannelCreatedTime  string
	CallerChannelProgressTime string
	CallerChannelMediaTime    string
	CallerChannelAnsweredTime string
	CallerChannelBridgedTime  string
	CallerChannelTransferTime string
	CallerChannelHangupTime   string
	AccountID                 string
	TrackID                   string
	BridgeAUUID				  string
	BridgeBUUID 			  string
    ChannelState              string
    ChannelCallState          string
    CallerOrigCallerIDName    string
    CallerOrigCallerIDNum     string
    CallerANI                 string
    CallerRDNIS               string
    OtherDirection            string
    OtherUsername             string
    OtherCallerIDName         string
    OtherCallerIDNum          string
    OtherOrigCallerIDName     string
    OtherOrigCallerIDNum      string
    OtherANI                  string
    OtherDestNum              string
    OtherRDNIS                string
	OtherLegUUID              string
    Application               string
    ApplicationData           string
	DTMFDigit                 string
}{
	"Event-Name",
	"FreeSWITCH-IPv4",
	"Unique-ID",
	"Answer-State",
	"Call-Direction",
    "Caller-Direction",
	"Caller-Context",
	"Caller-Destination-Number",
	"Caller-Callee-ID-Number",
	"Caller-Callee-ID-Name",
	"Caller-Caller-ID-Number",
	"Caller-Caller-ID-Name",
    "Caller-Username",
	"variable_direction",
	"variable_sip_req_host",
	"variable_sip_req_user",
	"variable_sip_from_host",
	"variable_sip_from_user",
	"variable_sip_from_display",
	"variable_sip_to_host",
	"variable_sip_to_user",
	"variable_sip_user_agent",
	"variable_effective_caller_id_number",
	"variable_hangup_cause",
	"variable_sip_hangup_disposition",
	"variable_bridge_hangup_cause",
	"Event-Date-Timestamp",
	"Caller-Channel-Created-Time",
	"Caller-Channel-Progress-Time",
	"Caller-Channel-Progress-Media-Time",
	"Caller-Channel-Answered-Time",
	"Caller-Channel-Bridged-Time",
	"Caller-Channel-Transfer-Time",
	"Caller-Channel-Hangup-Time",
	"variable_H2O_ACCOUNT_ID",
	"variable_H2O_TRACK_ID",
	"Bridge-A-Unique-ID",
	"Bridge-B-Unique-ID",
    "Channel-State",
    "Channel-Call-State",
    "Caller-Orig-Caller-ID-Name",
    "Caller-Orig-Caller-ID-Num",
    "Caller-ANI",
    "Caller-RDNIS",
    "Other-Leg-Direction",
    "Other-Leg-Username",
    "Other-Leg-Caller-ID-Name",
    "Other-Leg-Caller-ID-Number",
    "Other-Leg-Orig-Caller-ID-Name",
    "Other-Leg-Orig-Caller-ID-Number",
    "Other-Leg-ANI",
    "Other-Leg-Destination-Number",
    "Other-Leg-RDNIS",
	"Other-Leg-Unique-ID",
    "Application",
    "Application-Data",
	"DTMF-Digit",
}

// ESLVals defines some of the fixed set of event header values
var ESLVals = struct {
	CallDirectionInbound  string
	CallDirectionOutbound string
	AnswerStateRinging    string
	AnswerStateAnswered   string
	AnswerStateHangup     string
}{
	"inbound",
	"outbound",
	"ringing",
	"answered",
	"hangup",
}

// ESLEventName xxx
type ESLEventName = string

// ESLEventNames lists all the values for the Event-Name event variable
var ESLEventNames = struct {
	ChannelCreate          ESLEventName
	ChannelClose           ESLEventName
	ChannelDestroy         ESLEventName
	ChannelState           ESLEventName
	ChannelAnswer          ESLEventName
	ChannelHangup          ESLEventName
	ChannelHangupComplete  ESLEventName
	ChannelExecute         ESLEventName
	ChannelExecuteComplete ESLEventName
	ChannelBridge          ESLEventName
	ChannelUnbridge        ESLEventName
	ChannelProgress        ESLEventName
	ChannelProgressMedia   ESLEventName
	ChannelOutgoing        ESLEventName
	ChannelPark            ESLEventName
	ChannelUnpark          ESLEventName
	ChannelHold            ESLEventName
	ChannelUnhold          ESLEventName
	ChannelOriginate       ESLEventName
	ChannelUUID            ESLEventName
	ChannelCallState       ESLEventName
	Custom                 ESLEventName
	OutboundChan           ESLEventName
	InboundChan            ESLEventName
	RecordStart            ESLEventName
	RecordStop             ESLEventName
	PlaybackStart          ESLEventName
	PlaybackStop           ESLEventName
	MediaBugStart          ESLEventName
	MediaBugStop           ESLEventName
	CallUpdate             ESLEventName
	DetectedTone           ESLEventName
	DTMF                   ESLEventName
	Heartbeat              ESLEventName
	Shutdown               ESLEventName
	All                    ESLEventName
}{
	"CHANNEL_CREATE",
	"CHANNEL_CLOSE",
	"CHANNEL_DESTROY",
	"CHANNEL_STATE",
	"CHANNEL_ANSWER",
	"CHANNEL_HANGUP",
	"CHANNEL_HANGUP_COMPLETE",
	"CHANNEL_EXECUTE",
	"CHANNEL_EXECUTE_COMPLETE",
	"CHANNEL_BRIDGE",
	"CHANNEL_UNBRIDGE",
	"CHANNEL_PROGRESS",
	"CHANNEL_PROGRESS_MEDIA",
	"CHANNEL_OUTGOING",
	"CHANNEL_PARK",
	"CHANNEL_UNPARK",
	"CHANNEL_HOLD",
	"CHANNEL_UNHOLD",
	"CHANNEL_ORIGINATE",
	"CHANNEL_UUID",
	"CHANNEL_CALL_STATE",
    "CUSTOM",
	"OUTBOUND_CHAN",
	"INBOUND_CHAN",
	"RECORD_START",
	"RECORD_STOP",
	"PLAYBACK_START",
	"PLAYBACK_STOP",
	"MEDIA_BUG_START",
	"MEDIA_BUG_STOP",
	"CALL_UPDATE",
	"DETECTED_TONE",
	"DTMF",
	"HEARTBEAT",
	"SHUTDOWN",
	"ALL",
}

// GetStr xxx
func (e *ESLEvent) GetStr(key, dflt string) string {
	if val, found := e.Headers[key]; found {
		return val
	}
	return dflt
}

// GetInt xxx
func (e *ESLEvent) GetInt(key string, dflt int) int {
	if strVal, found := e.Headers[key]; found {
		if intVal, err := strconv.Atoi(strVal); err == nil {
			return intVal
		}
	}
	return dflt
}

func (e *ESLEvent) GetI64(key string, dflt int64) int64 {
	if strVal, found := e.Headers[key]; found {
		if int64Val, err := strconv.ParseInt(strVal, 10, 64); err == nil {
			return int64Val
		}
	}
	return dflt
}

func (e *ESLEvent) String() string {
	return fmt.Sprintf("Name: %s", e.Name)
}

/****************** Requests *********************/

// ESLRequest xxx
type ESLRequest interface {
	String() string
	Timeout() time.Duration
	Done() chan *ESLResult
	SetTimeout(timeoutSecs time.Duration)
}

// ESLRequestFields xxx
type ESLRequestFields struct {
	ESLRequest
	timeoutSecs time.Duration
	done chan *ESLResult
}
func (rf *ESLRequestFields) initRequestFields() {
	rf.done = make(chan *ESLResult)
}
// Done xxx
func (rf *ESLRequestFields) Done() chan *ESLResult {
	return rf.done
}
// Timeout xxx
func (rf *ESLRequestFields) Timeout() time.Duration {
	return rf.timeoutSecs
}

// ESLAPIRequest xxx
type ESLAPIRequest struct {
	ESLRequestFields
	command string
	arguments []string
}

// ESLEventRequest xxx
type ESLEventRequest struct {
	ESLRequestFields
	events []ESLEventName
	add bool
}

// ESLFilterRequest xxx
type ESLFilterRequest struct {
	ESLRequestFields
	name  string
	value string
	add   bool
}

// ESLSendMsg xxx
type ESLSendMsg struct {
	ESLRequestFields
	uuid string
	command string

}
// ESLExecuteRequest xxx
type ESLExecuteRequest struct {
	ESLRequestFields
	uuid string
	command string
	arguments []string
}

// NewAPIRequest xxx
func NewAPIRequest(command string, args []string) *ESLAPIRequest {
	req := &ESLAPIRequest{command: command, arguments: args}
	req.initRequestFields()
	if req.arguments == nil {
		req.arguments = make([]string, 0)
	}
	return req
}

// NewExecuteRequest xxx
func NewExecuteRequest(uuid, command string, arguments []string) *ESLExecuteRequest {
	req := &ESLExecuteRequest{uuid: uuid, command: command, arguments: arguments}
	req.initRequestFields()
	if req.arguments == nil {
		req.arguments = make([]string, 0)
	}
	return req
}

// NewFilterRequest xxx
func NewFilterRequest(name, value string, add bool) *ESLFilterRequest {
	req := &ESLFilterRequest{name: name, value: value, add: add}
	req.initRequestFields()
	return req
}

// NewEventRequest xxx
func NewEventRequest(events map[ESLEventName]bool, add bool) *ESLEventRequest {
	req := &ESLEventRequest{events: make([]ESLEventName, 0, len(events)), add: add}
	req.initRequestFields()
	for en, _ := range events {
		req.events = append(req.events, en)
	}
	return req
}

func (e *ESLAPIRequest) String() string {
	return fmt.Sprintf("api %s %s", e.command, strings.Join(e.arguments, " "))
}

func (e *ESLExecuteRequest) String() string {
	return fmt.Sprintf("sendmsg %s\ncall-command: execute\nexecute-app-name: %s\nexecute_app-arg: %s\n\n",
		e.uuid, e.command, strings.Join(e.arguments, " "))
}

func (e *ESLEventRequest) String() string {
	events := make([]string, 0, len(e.events))
	for _, en := range e.events {
		events = append(events, string(en))
	}
	op := "plain"
	if !e.add {
		op = "remove"
	}
	return fmt.Sprintf("event %s %s", op, strings.Join(events, " "))
}

func (e *ESLFilterRequest) String() string {
	if e.add {
		return fmt.Sprintf("filter %s %s", e.name, e.value)
	} else {
		return fmt.Sprintf("filter delete %s %s", e.name, e.value)
	}
}




